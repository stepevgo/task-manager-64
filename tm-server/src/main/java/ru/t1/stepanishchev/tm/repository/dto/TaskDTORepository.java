package ru.t1.stepanishchev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.stepanishchev.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
public interface TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findByUserId(@NotNull final String userId);

    @Nullable
    TaskDTO findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

    @Nullable
    @Query("SELECT p FROM TaskDTO p WHERE p.userId = :userId ORDER BY :sortType")
    List<TaskDTO> findAllByUserIdAndSort(@NotNull @Param("userId") String userId, @NotNull @Param("sortType") String sortType);

    void deleteByProjectId(@NotNull final String projectId);

    void deleteByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    long countByUserId(@NotNull final String userId);

}