package ru.t1.stepanishchev.tm.api.service.dto;

import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.dto.model.AbstractUserOwnedModelDTO;

@Service
public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IAbstractDTOService<M> {

}
