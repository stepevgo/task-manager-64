package ru.t1.stepanishchev.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.stepanishchev.tm.dto.model.AbstractUserOwnedModelDTO;

@Repository
public interface AbstractUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends AbstractDTORepository<M> {

}
