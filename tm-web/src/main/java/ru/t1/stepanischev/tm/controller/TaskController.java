package ru.t1.stepanischev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.stepanischev.tm.enumerated.Status;
import ru.t1.stepanischev.tm.model.TaskWeb;
import ru.t1.stepanischev.tm.repository.ProjectWebRepository;
import ru.t1.stepanischev.tm.repository.TaskWebRepository;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    private ProjectWebRepository projectRepository;

    @NotNull
    @Autowired
    private TaskWebRepository taskRepository;

    @GetMapping("/task/create")
    public String create() {
        taskRepository.save(new TaskWeb("New Task " + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskRepository.removeById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") TaskWeb task,
            BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        @Nullable final TaskWeb task = taskRepository.findById(id);
        @NotNull ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", Status.values());
        modelAndView.addObject("projects", projectRepository.findAll());
        return modelAndView;
    }

}
